---
title: Qui suis-je ?
subtitle: Académisme et fantaisie
comments: false
---

Mon surnom est Giloop. Parmi mes qualités assumées il y a : 

- I rock a great beard
- I'm extremely loyal to my friends
- I like bowling

That rug really tied the room together.

### my history

To be honest, I'm having some trouble remembering right now, so why don't you
just watch [my movie](https://en.wikipedia.org/wiki/The_Big_Lebowski) and it
will answer **all** your questions.
